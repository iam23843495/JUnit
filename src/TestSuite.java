import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by gerard on 28/03/17.
 */


@RunWith(value = Suite.class)
@Suite.SuiteClasses({
        CalculatorTest.class,
        ParameterizedCalculatorTest.class,
})
public class TestSuite {
}
