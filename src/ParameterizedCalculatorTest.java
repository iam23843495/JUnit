import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * Created by gerard on 27/03/17.
 */

@RunWith(value = Parameterized.class)
public class ParameterizedCalculatorTest {

    int a, b, expected;
    Calculator calc = new Calculator();

    public ParameterizedCalculatorTest(int a, int b, int expected) {
        this.a = a;
        this.b = b;
        this.expected = expected;
    }

    @Parameters
    public static Iterable<Object[]> parameters() {
        return Arrays.asList(new Object[][]{
                {5, 6, 11},
                {8, 4, 10},
                {5, 3, 8}});
    }

    @Test
    public void test() {
        assertEquals(calc.sum(a, b), expected);
    }
}