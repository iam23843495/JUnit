import org.junit.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by gerard on 28/03/17.
 */
public class OrderedTest {

    @BeforeClass
    public static void beforeClassTest() {
        System.out.println("THIS IS BEFORE ANY OTHER METHOD");
    }

    @Before
    public void beforeTest() {
        System.out.println("THIS IS BEFORE ANY TEST");
    }

    @Test
    public void test1() {
        assertEquals(1,1);
        System.out.println("THIS IS TEST 1");
    }

    @Test
    public void test2() {
        assertEquals(1,1);
        System.out.println("THIS IS TEST 2");
    }

    @After
    public void afterTest() {
        System.out.println("THIS IS AFTER ANY TEST");
    }

    @AfterClass
    public static void afterClassTest() {
        System.out.println("THIS IS AFTER ALL METHODS");
    }

}
