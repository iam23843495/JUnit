import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by gerard on 27/03/17.
 */
public class CalculatorTest {
    Calculator calculator = new Calculator();

    @Test
    public void sumTest() {
        assertEquals(calculator.sum(5,9), 14);
    }

    @Test
    public void sumTestFail() {
        assertEquals(calculator.sum(7, 19), 12);
    }

    @Test(expected = ArithmeticException.class)
    public void expectedTest() {
        int x = 0 / 1;
    }

    @Test(timeout = 100)
    public void timeoutTest() {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



}